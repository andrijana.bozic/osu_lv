import numpy as np
from tensorflow import keras
from matplotlib import pyplot as plt
from keras.models import load_model

num_classes = 10
input_shape = (28, 28, 1)

(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

print('Train: X=%s, y=%s' % (x_train.shape, y_train.shape))
print('Test: X=%s, y=%s' % (x_test.shape, y_test.shape))

x_train_s = x_train.astype("float32") / 255
x_test_s = x_test.astype("float32") / 255

x_train_s = np.expand_dims(x_train_s, -1)
x_test_s = np.expand_dims(x_test_s, -1)

print("x_train shape:", x_train_s.shape)
print(x_train_s.shape[0], "train samples")
print(x_test_s.shape[0], "test samples")

y_train_s = keras.utils.to_categorical(y_train, num_classes)
y_test_s = keras.utils.to_categorical(y_test, num_classes)

x_train_reshape = np.reshape(x_train, (60000, 784))
x_test_reshape = np.reshape(x_test, (10000, 784))

model = load_model("LV8model.keras")

predictions = model.predict(x_test_reshape)

y_true = np.argmax(y_test_s, axis=-1)
y_pred = np.argmax(predictions, axis=-1)

if(y_true != y_pred).any():
    for i in range(5):
        plt.imshow(x_train[i], cmap ="gray")
        plt.title(f"Stvarna oznaka: {y_train[i]}, Predvidjena oznaka: {y_pred[i]}")
        plt.show()