import numpy as np
from matplotlib import pyplot as plt
from keras.models import load_model
from PIL import Image


model = load_model("LV8model.keras")

img = Image.open("test2.png").convert("L")
#convert("L") kako bismo pretvorili sliku u crno-bijelu verziju(grayscale)
img = img.resize((28, 28))

img_array = np.array(img)
img_array = img_array.astype("float32") / 255
img_array = np.expand_dims(img_array, -1)
img_array = np.reshape(img_array, (1, 784))
#Prva dimenzija (1) oznacava broj primjera ili uzoraka. Ovdje je postavljena na 1 jer se radi samo o jednoj slici.
#Druga dimenzija (784) oznacava duljinu 1d niza koji se dobije spajanjem svih piksela slike u jedan niz.

plt.figure()
plt.imshow(img)
plt.show()

prediction = model.predict(img_array)
y_pred = np.argmax(prediction)
print(y_pred)

#Ako ne napisemo jasno citljivo neku znamenku, model moze napraviti pogresku u klasifikaciji 

