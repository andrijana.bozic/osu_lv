import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score, ConfusionMatrixDisplay


# Model / data parameters
num_classes = 10
input_shape = (28, 28, 1)#(visina, sirina, dubina)
#Visina i sirina (28x28) predstavljaju dimenzije slike, sto znaci da su ulazni podaci matrica dimenzija 28 piksela po visini i 28 piksela po 
#sirini. Dubina (1) oznacava broj kanala slike. U ovom slucaju, dubina je 1, sto ukazuje na to da je slika u crno-bijelom formatu, tj. ima 
#samo jedan kanal.

# train i test podaci
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

# prikaz karakteristika train i test podataka
print('Train: X=%s, y=%s' % (x_train.shape, y_train.shape))
print('Test: X=%s, y=%s' % (x_test.shape, y_test.shape))

# TODO: prikazi nekoliko slika iz train skupa
for i in range(3):
    print("Label", y_train[i])
    #y_train je lista koja sadrzi oznake(brojeve od 0 do 9) koji predstavljaju znamenke koje odgovaraju slikama u x_train
    plt.imshow(x_train[i], cmap ="gray")
    plt.show()
    
# skaliranje slike na raspon [0,1]
x_train_s = x_train.astype("float32") / 255
x_test_s = x_test.astype("float32") / 255

# slike trebaju biti (28, 28, 1)
x_train_s = np.expand_dims(x_train_s, -1)
x_test_s = np.expand_dims(x_test_s, -1)
#Funkcija np.expand_dims() koristi se u biblioteci NumPy kako bi se prosirile dimenzije niza. Kada se koristi, dodaje se nova osa na odredjenom
#polozaju u nizu. Argument -1 znaci da ce nova osa biti dodana na posljednje mjesto u obliku niza.

print("x_train shape:", x_train_s.shape)
print(x_train_s.shape[0], "train samples")
print(x_test_s.shape[0], "test samples")

# pretvori labele
y_train_s = keras.utils.to_categorical(y_train, num_classes)
y_test_s = keras.utils.to_categorical(y_test, num_classes)

# TODO: kreiraj model pomocu keras.Sequential(); prikazi njegovu strukturu
model = keras.Sequential()
model.add(layers.Input(shape =(784, )))#Svaki piksel moze se smatrati znacajkom.
model.add(layers.Dense(100, activation ="relu"))
model.add(layers.Dense(50, activation ="relu"))
model.add(layers.Dense(10, activation ="softmax"))
#izlazni sloj sadrzi 10 neurona jer ukupno postoji 10 klasa (znamenki)

model.summary()

# TODO: definiraj karakteristike procesa ucenja pomocu .compile()
model.compile(loss ="categorical_crossentropy", optimizer ="adam", metrics =["accuracy",])

# TODO: provedi ucenje mreze
x_train_reshape = np.reshape(x_train, (60000, 784))
#Broj 60000 oznacava ukupan broj slika u skupu za treniranje dok broj 784 oznacava duljinu svakog vektora nakon preoblikovanja
x_test_reshape = np.reshape(x_test, (10000, 784))

batch_size = 32
epochs = 4
history = model.fit(x_train_reshape, y_train_s, batch_size = batch_size, epochs = epochs, validation_split = 0.1)

score = model.evaluate(x_test_reshape, y_test_s, verbose = 0)
#verbose kontrolira koliko ce informacija biti prikazano tijekom evaluacije modela
predictions = model.predict(x_test_reshape)

# TODO: Prikazi test accuracy i matricu zabune
y_true = np.argmax(y_test_s, axis=-1)
y_pred = np.argmax(predictions, axis=-1)
print("Tocnost:" ,accuracy_score(y_true, y_pred))

cm = confusion_matrix(y_true, y_pred)
print("Matrica zabune:\n" ,cm)
disp = ConfusionMatrixDisplay(confusion_matrix(y_true, y_pred))
disp.plot()
plt.show()

# TODO: spremi model
model.save("LV8model.keras")
del model

#1
#Skup za ucenje sadrzi 60000 primjera, a skup za testiranje 10000.
#Ulazni podaci tj. slike su skalirani na raspon [0,1].
#Izlazne velicine(y_train i y_test) su kodirane 1-od-K kodiranjem.