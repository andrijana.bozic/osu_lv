ocjena = ""
while ocjena == "":
    try:
        print("Unesite broj koji predstavlja ocjenu izmedju 0.0 i 1.0:")
        ocjena = float(input())
    except:
        print("Greska, niste unijeli broj!")
        continue

while ocjena < 0.0 or ocjena > 1.0:
    try:
        print("Broj nije u zadanom intervalu, unesite ponovo:")
        ocjena = float(input())
    except:
        print("Greska, niste unijeli broj!")
        continue

if ocjena >= 0.9:
    print("A")
elif ocjena >= 0.8:
    print("B")
elif ocjena >= 0.7:
    print("C")
elif ocjena >= 0.6:
    print("D")
elif ocjena < 0.6:
    print("F")
