data_file = open("song.txt", "r")
dictionary = {}
keys = []
values = []
counter_key_equal_to_word = 0
all_words_list = []
counter_value_is_one = 0

for line in data_file :
    line = line.rstrip()
    line = line.lower()
    words = line.split()
    all_words_list.extend(words)
    words_string = str(words)
    words_string = words_string.replace(',', '')
    commas_added_string = ', '.join(words_string.split())
    new_list = eval(commas_added_string)
    
    brojac = 0
    for word in new_list:
        for key in keys:
            if word == key:
                brojac = brojac + 1
        if brojac == 0:
            keys.append(word)
        else:
            brojac = 0
 

for key in keys:
    for word in all_words_list:
        if key == word:
            counter_key_equal_to_word = counter_key_equal_to_word + 1
    values.append(counter_key_equal_to_word) 
    counter_key_equal_to_word = 0                  
            
    
data_file.close()
       
for value in values:
    if value == 1:
        counter_value_is_one = counter_value_is_one + 1


dictionary = dict(zip(keys, values))         
print("Rijecnik je:", dictionary)

print("Broj rijeci koje se pojavljuju samo jednom u datoteci je:", counter_value_is_one)
            

