from numpy import mean

lista_brojeva = []
unos_broja = ""
while unos_broja != "Done":
    print("Unesite broj:")
    unos_broja = input()
    if unos_broja != "Done":
        try:
            unos_broja = float(unos_broja)
            lista_brojeva.append(unos_broja)
        except:
            print("GRESKA-Nije uneseno ni slovo, ni Done.")
            continue

print("Korisnik je unio", len(lista_brojeva), "brojeva")
print("Njihova srednja vrijednost:", mean(lista_brojeva))
print("Njihova minimalna vrijednost:", min(lista_brojeva))
print("Njihova maksimalna vrijednost", max(lista_brojeva))

lista_brojeva.sort()

print("Lista sortiranih brojeva:")
for broj in lista_brojeva:
    print(broj)    
