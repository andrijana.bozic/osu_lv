data = open('SMSSpamCollection.txt', encoding="utf8")
lines_ham = 0
lines_spam = 0
count_words_hamm = 0
count_words_spam = 0
count_exclamation_mark = 0

for line in data:
       line = line.strip()
       line = line.lower()
       words = line.split()

       if words[0] == "ham":
            count_words_hamm = count_words_hamm + (len(words)-1)
            lines_ham = lines_ham + 1
       elif words[0] == "spam":
            count_words_spam = count_words_spam + (len(words)-1)
            lines_spam = lines_spam + 1
            if words[-1].endswith('!'):
                    count_exclamation_mark = count_exclamation_mark + 1

data.close()
                   
print('The average number of words in messages that are of the ham type is:', count_words_hamm/lines_ham)
print('The average number of words in messages that are of the spam type is:', count_words_spam/lines_spam)
print('The number of words ending with an exclamation mark in spam messages:', count_exclamation_mark)