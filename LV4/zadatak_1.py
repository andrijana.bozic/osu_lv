import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_absolute_percentage_error
from sklearn.metrics import r2_score

data = pd.read_csv('data_C02_emission.csv')

#a
X = data[['Engine Size (L)', 'Cylinders', 'Fuel Consumption City (L/100km)', 'Fuel Consumption Hwy (L/100km)', 'Fuel Consumption Comb (L/100km)', 'Fuel Consumption Comb (mpg)']]
y = data['CO2 Emissions (g/km)'].copy()
X_train, X_test, y_train, y_test = train_test_split (X, y, test_size = 0.2, random_state = 1)

#b
for column in X_train.columns:
    plt.scatter(X_train[column], y_train, c = 'blue', label = 'Train')
    plt.scatter(X_test[column], y_test, c = 'red', label = 'Test')
    plt.xlabel(column)
    plt.ylabel('CO2 Emissions (g/km)')
    plt.title('Dijagram rasprsenja')
    plt.legend()
    plt.show()
    
#c
sc = MinMaxScaler()
X_train_n = pd.DataFrame(sc.fit_transform(X_train), columns = X_train.columns, index = X_train.index)
X_test_n = pd.DataFrame(sc.transform(X_test), columns = X_test.columns, index = X_test.index)
for column in X_train.columns:
    fig, ax = plt.subplots(2, figsize = (6, 6))
    ax[0].hist(X_train[column])
    ax[0].set_xlabel(column)
    ax[0].set_title('Prije skaliranja')
    ax[1].hist(X_train_n[column])
    ax[1].set_xlabel(column)
    ax[1].set_title('Poslije skaliranja')
    plt.tight_layout()
    plt.show()    
    
#d
print('\nd)')
linearModel = lm.LinearRegression()
linearModel.fit(X_train_n, y_train)
print(linearModel.coef_)
print(linearModel.intercept_)

#e
y_test_p = linearModel.predict(X_test_n)
plt.scatter(y_test, y_test_p)
plt.title('Odnos izmedju stvarnih vrijednosti izlazne velicine i procjene dobivene modelom')
plt.show()

#f
print('\nf)')
MSE = mean_squared_error(y_test, y_test_p)
print('Srednja kvadratna pogreška (engl. mean squared error - MSE):', MSE)
RMSE = np.sqrt(MSE)
print('Korijen iz srednje kvadratne pogreške (engl. root mean squared error - RMSE):', RMSE)
MAE = mean_absolute_error(y_test, y_test_p)
print('Srednja apsolutna pogreška (engl. mean absolute error - MAE):', MAE)
MAPE = mean_absolute_percentage_error(y_test, y_test_p)
print('Srednja apsolutna postotna pogreška (engl. mean absolute percentage error - MAPE):', MAPE)
R2 = r2_score(y_test, y_test_p)
print('Koeficijent determinacije R2', R2)

#g
print('\ng)')
print('Kada mijenjam broj ulaznih velicina, MSE se smanjuje, a R2 povecava.')