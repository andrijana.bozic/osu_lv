import pandas as pd
import matplotlib. pyplot as plt
import numpy as np

data = pd.read_csv('data_C02_emission.csv')

#a
print('\na)')
plt.figure()
data['CO2 Emissions (g/km)'].plot(kind = 'hist', bins = 40)
print('Vidimo da je frekvencija emisije C02 plinova u gramima po kilometru za kombiniranu vožnju najveca izmedju 198 i 324 g/km.')
print('A od tih vrijednosti, najvise se istice frekvencija izmedju vrijednosti 210 i 220 g/km.')

#b
print('\nb)')
data['Make']=pd.Categorical(data['Make'])
data["Model"] = pd.Categorical(data["Model"])
data['Vehicle Class']=pd.Categorical(data['Vehicle Class'])
data['Transmission']=pd.Categorical(data['Transmission'])
data['Fuel Type']=pd.Categorical(data['Fuel Type'])

data.plot.scatter(x = 'Fuel Consumption City (L/100km)', y = 'CO2 Emissions (g/km)', c = 'Fuel Type', cmap = 'plasma', s = 15)

print('Mozemo zakljuciti da sto je veca gradska potrosnja goriva, veca je i emisija CO2 plinova.')

#c
print('\nc)')
grouped_fuel_types = data.groupby('Fuel Type', observed = False)
grouped_fuel_types.boxplot(column = ['Fuel Consumption Hwy (L/100km)'])
plt.tight_layout()
print('U Z kutijastom dijagramu primjecujem puno outliera iznad gornjeg kvartila.')

#d
print('\nd)')
plt.figure()
grouped_fuel_types_size = data.groupby('Fuel Type', observed=False).size()
grouped_fuel_types_size.plot(kind='bar')

#e
print('\ne)')
plt.figure()
grouped_cylinders = data.groupby('Cylinders')['CO2 Emissions (g/km)'].mean()
grouped_cylinders.plot(kind = 'bar')
plt.show()
