import pandas as pd

data = pd.read_csv('data_C02_emission.csv')

#a
print('\na)')
print('DataFrame sadrzi', len(data), 'mjerenja.')
print('Tipovi velicina:\n')
print(data.dtypes, '\n')
print(data.isnull().sum(), '\n')
print(data.duplicated().sum(), '\n')
data['Make'] = pd.Categorical(data.Make)
data['Model'] = pd.Categorical(data.Model)
data['Vehicle Class'] = pd.Categorical(data['Vehicle Class'])
data['Transmission'] = pd.Categorical(data.Transmission)
data['Fuel Type'] = pd.Categorical(data['Fuel Type'])

#b
print('\nb)')
data_sorted =  data.sort_values(by = ['Fuel Consumption City (L/100km)'])
print('Tri automobila s najvecom gradskom potrosnjom su:\n')
print(data_sorted[['Make', 'Model', 'Fuel Consumption City (L/100km)']].tail(3), '\n')
print('Tri automobila s najmanjom gradskom potrosnjom su:\n')
print(data_sorted[['Make', 'Model', 'Fuel Consumption City (L/100km)']].head(3), '\n')

#c
print('\nc)')
print('Broj vozila koja imaju veličinu motora između 2.5 i 3.5 je',len(data [(data['Engine Size (L)'] > 2.5 ) & (data['Engine Size (L)'] < 3.5)]))
mean_co2_emission = data.loc[(data['Engine Size (L)'] > 2.5 ) & (data['Engine Size (L)'] < 3.5), 'CO2 Emissions (g/km)'].mean()
print('Prosjecna C02 emisija plinova za ova vozila:',mean_co2_emission, '\n') 

#d
print('\nd)')
print('Broj mjerenja koja se odnose na vozila proizvodjaca Audi', len(data[data.Make == 'Audi']))
mean_Audi_4cylinders = data.loc[(data['Make'] == 'Audi') & (data['Cylinders'] == 4), 'CO2 Emissions (g/km)'].mean()
print('Prosjecna emisija C02 plinova automobila proizvodjaca Audi koji imaju 4 cilindara je:', mean_Audi_4cylinders)

#e
print('\ne)')
cylinders_grouped = data.groupby('Cylinders')
print(cylinders_grouped.count())
print('Prosjecna emisija C02 plinova s obzirom na broj cilindara:',data.groupby('Cylinders')['CO2 Emissions (g/km)'].mean())

#f
print('\nf)')
mean_diesel_fuel_consumption = data.loc[(data['Fuel Type'] == 'D'), 'Fuel Consumption City (L/100km)'].mean()
print('Prosjecna gradska potrošnja u slucaju vozila koja koriste dizel:',mean_diesel_fuel_consumption)
mean_regular_gasoline_fuel_consumption = data.loc[(data['Fuel Type'] == 'X'), 'Fuel Consumption City (L/100km)'].mean()
print('Prosjecna gradska potrošnja u slucaju vozila koja koriste regularni benzin:',mean_regular_gasoline_fuel_consumption)

#g
print('\ng)')
print('Vozilo s 4 cilindra koje koristi dizelski motor i ima najvecu gradsku potrošnju goriva:')
print(data[(data['Cylinders'] == 4) & (data['Fuel Type'] == 'D')].sort_values(by = ['Fuel Consumption City (L/100km)']).tail(1))

#h
print('\nh)')
print('Broj vozila koja imaju rucni tip mjenjaca:', data['Transmission'].str.startswith('M').sum())

#i
print('\ni)')
print(data.corr(numeric_only = True))
print('Najvise vrijednosti ima pozitivnu korelaciju, što znači da se vrijednosti jednog stupca povećavaju kako se povećavaju vrijednosti drugog stupca.')
print('Nekoliko od tih vrijednosti ima korelacija koja iznosi tocno 1.0.')


