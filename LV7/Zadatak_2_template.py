import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans
from sklearn.datasets import make_blobs

# ucitaj sliku
img = Image.imread("imgs\\test_1.jpg")

# prikazi originalnu sliku
plt.figure()
plt.title("Originalna slika")
plt.imshow(img)
plt.tight_layout()
plt.show()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img = img.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w,h,d = img.shape
img_array = np.reshape(img, (w*h, d))
#transformira originalnu matricu slike u jednodimenzionalni niz, gdje svaki red predstavlja jedan piksel u originalnoj slici
#w*h predstavlja ukupan broj piksela u slici, dok d predstavlja dubinu slike (broj kanala boja)

# rezultatna slika
img_array_aprox = img_array.copy()

#1
print("Broj razlicitih boja u originalnoj slici:", len(np.unique(img_array_aprox, axis=0)))

#2
# inicijalizacija algoritma K srednjih vrijednosti
km = KMeans(n_clusters = 5, init ='random', n_init =5, random_state =0)
# pokretanje grupiranja primjera
km.fit(img_array_aprox)
# dodijeljivanje grupe svakom primjeru
labels = km.predict(img_array_aprox)

#7
labels_unique = np.unique(labels)
for i in range(len(labels_unique)):
    binary_image = labels==labels_unique[i]
    #pravi binarnu sliku postavljanjem vrijednosti piksela na True tamo gdje se etiketa klastera u nizu labels podudara sa trenutnom 
    #jedinstvenom etiketom klastera labels_unique[i]
    binary_image = np.reshape(binary_image, (w,h))
    plt.figure()
    plt.title(f"Binarna slika {i+1}. grupe boja")
    plt.imshow(binary_image)
    plt.tight_layout
    plt.show()
#Prikazom binarnih slika svake grupe, primjecujem drukciji raspored boja u svakoj grupi.

#3
for i in range(len(labels)):
    img_array_aprox[i] = km.cluster_centers_[labels[i]]
    # kvantizacija boja, gdje se svi pikseli u slici zamjenjuju centrima klastera na osnovu njihovih oznaka klastera
print("Broj razlicitih boja u kvantiziranoj slici:", len(np.unique(img_array_aprox, axis=0)))

#4
# prikazi kvantiziranu sliku
img_array_aprox = np.reshape(img_array_aprox, (w,h,d))#transformira 1D niz u originalni oblik
img_array_aprox = (img_array_aprox*255).astype(np.uint8)#sve vrijednosti piksela u slici img_aprox u opsegu od 0 do 255 i predstavljene su
#kao 8-bitni bez znaka cjelobrojni brojevi
plt.figure()
plt.title("Kvantizirana slika")
plt.imshow(img_array_aprox)
plt.tight_layout()
plt.show()
print("\nSto je K manji, slika je sve losija od originalne.")

#5
from PIL import Image
for i in range(2,6):
    img_path = "imgs\\test_" + str(i) + ".jpg"  
    img2 = Image.open(img_path)
      
    # prikazi originalnu sliku
    plt.figure()
    plt.title("Originalna slika")
    plt.imshow(img2)
    plt.tight_layout()
    plt.show()

    # Pretvaranje slike u NumPy niz
    img_array2 = np.array(img2, dtype=np.float64) / 255.0

    # transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
    w,h,d = img_array2.shape
    img2_array = np.reshape(img_array2, (w*h, d))

    # rezultatna slika
    img2_array_aprox = img2_array.copy()

    print("\nBroj razlicitih boja u originalnoj slici:", len(np.unique(img2_array_aprox, axis=0)))

    # inicijalizacija algoritma K srednjih vrijednosti
    km2 = KMeans(n_clusters = 5, init ='random', n_init =5, random_state =0)

    # pokretanje grupiranja primjera
    km2.fit(img2_array_aprox)

    # dodijeljivanje grupe svakom primjeru
    labels2 = km2.predict(img2_array_aprox)
    for i in range(len(labels2)):
        img2_array_aprox[i] = km2.cluster_centers_[labels2[i]]
    print("Broj razlicitih boja u kvantiziranoj slici:", len(np.unique(img2_array_aprox, axis=0)))

    # prikazi kvantiziranu sliku
    img2_array_aprox = np.reshape(img2_array_aprox, (w,h,d))
    img2_array_aprox = (img2_array_aprox*255).astype(np.uint8)
    plt.figure()
    plt.title("Kvantizirana slika")
    plt.imshow(img2_array_aprox)
    plt.tight_layout()
    plt.show()

#6
# Generiranje slucajnog skupa podataka
X, _ = make_blobs(n_samples=300, centers=4, cluster_std=0.60, random_state=0)
#X predstavlja matricu sa uzorcima, dok y(_) predstavlja niz sa oznakama klastera za svaki uzorak.

# Lista inercija za razlicit broj klastera
inertia = []

# Testiranje KMeans algoritma sa razlicitim brojevima klastera
for i in range(1, 11):
    kmeans = KMeans(n_clusters=i, random_state=0)
    kmeans.fit(X)
    inertia.append(kmeans.inertia_)

# Prikazivanje rezultata na grafikonu
plt.plot(range(1, 11), inertia, marker='o')
plt.xlabel('Broj klastera')
plt.ylabel('Inercija')
plt.title('Elbow method')
plt.show()
#Uocava se nagli pad vrijednosti J za K = 3 sto je optimalni broj grupa
