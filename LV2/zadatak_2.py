import numpy as np
import matplotlib.pyplot as plt
from numpy import mean

data = np.loadtxt('data.csv', delimiter = ',', skiprows = 1)

print("Mjerenja su izvrsena na:",len(data[:, 0]), "osoba.")

plt.figure(0)
plt.scatter(data[:, 1], data[:, 2], color = 'green')
plt.xlabel('Height')
plt.ylabel('Weight')
plt.title('The relationship between height and mass of persons')

plt.figure(1)
plt.scatter(data[::50, 1], data[::50, 2], color = 'green')
plt.xlabel('Height')
plt.ylabel('Weight')
plt.title('The relationship between height and mass of every 50th person')
plt.show()

print('\nMin height:', min(data[:, 1]))
print('Max height:', max(data[:, 1]))
print('Mean height:', mean(data[:, 1]))

ind_men = (data[:, 0] == 1)
print('\nMin height for men:', min(data[ind_men, 1]))
print('Max height for men:', max(data[ind_men, 1]))
print('Mean height for men:', mean(data[ind_men, 1]))

ind_women = (data[:, 0] == 0)
print('\nMin height for women:', min(data[ind_women, 1]))
print('Max height for women:', max(data[ind_women, 1]))
print('Mean height for women:', mean(data[ind_women, 1]),'\n')