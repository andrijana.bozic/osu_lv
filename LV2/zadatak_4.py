import numpy as np
import matplotlib.pyplot as plt

white_array = np.ones((50, 50, 3), dtype=np.uint8) * 255
black_array = np.zeros((50, 50, 3), dtype=np.uint8)

img1= np.hstack([black_array, white_array])
img2 = np.hstack([white_array, black_array])
img = np.vstack([img1, img2])
plt.figure()
plt.imshow(img)
plt.title('Black white')
plt.show()