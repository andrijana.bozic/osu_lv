import numpy as np
import matplotlib.pyplot as plt

img = plt.imread("road.jpg")
img = img[:,:,0].copy()
print(img.shape)
print(img.dtype)

plt.figure()
plt.imshow(img, cmap ="gray")
plt.title('Original image')

brightness_factor = 0.1
brightened_img = np.clip(img * brightness_factor, 0, 12)
plt.figure()
plt.imshow(brightened_img, cmap ="gray")
plt.title('Brightened image')

second_quarter = 640//2 
img_second_quarter = img[:, second_quarter:]
plt.figure()
plt.imshow(img_second_quarter, cmap ="gray")
plt.title('Second quarter image')

img_rotated = np.rot90(img)
img_rotated = np.rot90(img_rotated)
img_rotated = np.rot90(img_rotated)
plt.figure()
plt.imshow(img_rotated, cmap ="gray")
plt.title('Rotated image')

img_mirrored = np.fliplr(img)
plt.figure()
plt.imshow(img_mirrored, cmap ="gray")
plt.title('Mirrored image')
plt.show()